import {displayResult} from '../src/displayResult'
import '../node_modules/bootstrap'

let mockRes = [];

beforeEach(()=>{
    mockRes=[];
})

jest.mock('handlebars',
    ()=>{
        return {
            compile: ()=>{
                return (d) => {
                    mockRes.push(d);
                };
            }
        }
    }
);


test('test of display result with null input', ()=>{
    displayResult(null, null);
    expect(mockRes).toEqual([{'message': 'result or location is null'}]);
});

mockRes = [];
jest.mock('handlebars',
    ()=>{
        return {
            compile: ()=>{
                return (d) => {
                    mockRes.push(d);
                };
            }
        }
    }
);

test('test of display result with city and country', ()=>{
    displayResult({
        city: {
            name: 'TestCity',
            country: 'TE'
        },
        list: [
            {
                dt_txt: '2018-08-14 18:00:00',
                weather: [{
                    icon: 'iconCode',
                    description: 'description'
                }],
                main: {
                    temp: 33.6,
                    pressure: 1020.4,
                    humidity: 88
                }
            }
        ]
    }, {});
    expect(mockRes).toEqual([{
        city: 'TestCity',
        country: 'TE',
        forecasts: [{
            datetime: '2018-08-14 18:00',
            description: 'description',
            humidity: 88,
            iconSrc: 'iconCode.png',
            pressure: 1020,
            temperature: 34,
            temperatureBacgroundColor: '#b10909',
            temperatureTextColor: 'white'
        }]
    }]);
});
