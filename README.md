WiPro-Digital intervie app
==========================

## PREREQUISITES

To be able to build this app You must have a node with npm or (better) yarn installed.

## Build and run 

To build or start development server You must first install all dependencies by running
```
npm install
```
or 
```
yarn
```

To run the app in development mode 
```
yarn run dev
```
or
```
npm run dev
```
And if the browser does not open automaticaly please manually open [this page](http://localhost:8080)  


To build production version 
```
yarn run build
```
or
```
npm run build
```
The app will be placed in the `dist/` folder and the zip archiwe will be created in the `dist/zip/` folder

To test run 
```
yarn run test
```
or
```
npm run test
```
 
## The app design
The actual design is very straightforward.
- `index.html` uses bootstrap and handlebars templates 
- `index.js` loads the google map and initializes click handler
- in `openweathermapClient.js` the actual client is defined that does the http request for 
    the whteher forecast data. 
-  `displayResult.js` does what the name suggests and does it with help of handlebar templates defined in `index.html`. It also display an error message if there is one.
- the app reports its version to browser javascript console when it loads
    
## Known issues
- the 'the-color-of-air' library only supports temperatures between -23 and 42 deg C. When air temperature is from outside this range the function calculating the color of the text fails. 
- the hardcoded value for timeout in `openweathermapClient.js`
- the Google Maps and OpenWheather api keys are hardcoded in the js sources 
- the content does a little jump to the right when modal with height extending the screen height is displayed. This is caused by appearing the scroll bar on the left
- not all of the obtained wheather parameter are presented 
- the alternative presentation of the parameters as diagrams could be nice      
- could be more tests (there always could be more)
- date is not aligned with wheather indicators results 