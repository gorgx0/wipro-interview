import 'jquery';
import {displayError} from './displayResult'

export function get5DaysForecastByLocation(latLng, callback) {
    $.ajax({
        dataType: 'json',
        url: 'http://api.openweathermap.org/data/2.5/forecast',
        data: {
            lat: latLng.lat(),
            lon: latLng.lng(),
            units: 'metric',
            APPID: '43086942c520bd86a87faa8b6b9420a5'
        },
        success: (res) => {
            callback(res, latLng);
        },
        timeout: 30000
    })
    .fail((jqXHR, textStatus, errorThrown)=>{
        displayError(errorThrown);
    });
}
