import 'bootstrap'
import './scss/index.scss'
import {get5DaysForecastByLocation} from './openweathermapClient'
import {displayResult} from './displayResult'

import loader from '../assets/loading.gif';

const GoogleMapsLoader = require('google-maps');
// GoogleMapsLoader.KEY = 'AIzaSyD0SPDfV03l9aTjW1CUPdfGlPgaygSNtb4';
GoogleMapsLoader.KEY = 'AIzaSyBmY88uO3Djrjd0kj6QMxMVz0WEiAdCiYo';

$(()=>{
    console.info('App loaded');
    console.info('Application version info');
    console.info(`Commit id: ${COMMITHASH}`);
    console.info(`Version: ${VERSION}`);
    console.info(`Branch : ${BRANCH}`);
    $('#forecastResult').modal('hide');

    GoogleMapsLoader.load(()=>{
        const warszawa = {lat: 52.232222, lng: 21.008333};

        const map = new google.maps.Map(
            document.getElementById('map'),
            {zoom: 4, center: warszawa, streetViewControl: false}
        );

        map.addListener('click', (clickEvent)=>{
            $('#forecastResult').off('shown.bs.modal');
            $('#forecastResult').on('shown.bs.modal', (modalShownEvent)=>{
                get5DaysForecastByLocation(clickEvent.latLng, displayResult);
            });
            $('#forecastResult').modal('show');
        });
    });
});
