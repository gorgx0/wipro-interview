import $ from 'jquery';
import {colorOfAir} from 'the-color-of-air';
import Handlebars from 'handlebars';

const errorTemplate = Handlebars.compile($('#genericError').html());
const resultTemplate = Handlebars.compile($('#results').html());

export function displayError(message) {
    if (message == null) {
        message = 'Something went wrong. Sorry.'
    }
    $('#forecastResult').modal('hide');
    $('#errorContainer').html('');
    $('#errorContainer').append(errorTemplate({message: message}));
}

export function displayResult(res, location) {
    if (res==null || location == null){
        displayError('result or location is null');
        return;
    }
    let data = {};
    if (res.city.hasOwnProperty('name') && res.city.hasOwnProperty('country')) {
        data.city = res.city.name;
        data.country = res.city.country;
    } else {
        data.lat = roundLocation(location.lat());
        data.lng = roundLocation(location.lng());
    }
    data.forecasts = res.list.map((value)=>{
        return {
            datetime: value.dt_txt.substr(0, value.dt_txt.length-3),
            iconSrc: value.weather[0].icon + '.png',
            description: value.weather[0].description,
            temperature: Math.round(value.main.temp),
            temperatureBacgroundColor: colorOfAir('c', value.main.temp),
            temperatureTextColor: getcontrast(colorOfAir('c', value.main.temp)),
            pressure: Math.round(value.main.pressure),
            humidity: value.main.humidity
        }
    });
    $('#resultContainer').html('');
    $('#resultContainer').append(resultTemplate(data));
}

function roundLocation(x) {
    return Number.parseFloat(x).toPrecision(5);
}

function getcontrast(hex) {
    const r = parseInt(hex.substr(1, 2), 16);
    const g = parseInt(hex.substr(3, 2), 16);
    const b = parseInt(hex.substr(5, 2), 16);
    const yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
    return (yiq >= 128) ? 'black' : 'white';
}
